package co.eshop.microcategoria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrocategoriaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrocategoriaApplication.class, args);
	}

}
